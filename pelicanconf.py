#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Mowee'
SITENAME = u'Mowee'
SITESUBTITLE = u'Pensées libres'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

LOCALE = 'fr_FR.UTF-8'

DEFAULT_DATE_FORMAT = '%d %B %Y'

STATIC_PATHS = ['images']

HEADER_COVER = 'images/camping.jpg'

DEFAULT_PAGINATION = 5

THEME = 'theme/pelican-clean-blog'
FAVICON = 'images/favicon.ico'

GITLAB_URL = 'https://framagit.org/Mowee'

TWITTER_URL = 'https://twitter.com/heyimmowee'
