Title: À propos
Date: 2019-03-10 10:20
Category: pages
Slug: a-propos
Summary: À propos du blog de Mowee

Hello ! Je suis Mowee, un développeur passionné par Python et son écosystème.

À travers ce blog, je partagerai mes réflexions techniques ou non ainsi que mes projets.

Ce blog fonctionne grâce au moteur [Pelican](https://blog.getpelican.com){:target="_blank"} créé par l'ami [Alexis](https://twitter.com/ametaireau){:target="_blank"}.

Il est hébergé sur [Framagit](https://framagit.org){:target="_blank"}, le Gitlab maintenu par l'association [Framasoft](https://framasoft.org){:target="_blank"}.

Il utilse le thème [pelican-clean-blog](https://github.com/gilsondev/pelican-clean-blog){:target="_blank"} que j'ai épuré pour être le moins dépendant de services externes (fonts, scripts...)

Bonne lecture !