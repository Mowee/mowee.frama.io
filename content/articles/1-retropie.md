Title: Installation de retropie sur un raspberry pi
Date: 2018-03-11 10:00
Tags: raspberry pi, retropie
Category: raspberry
Slug: installation-retropie-raspberry-pi
Summary: Ça fait longtemps que j'ai un raspberry pi 2 B+ qui traîne dans le placard. Ce n'est pas le dernier modèle...

Ça fait longtemps que j'ai un raspberry pi 2 B+ qui traîne dans le placard. Ce n'est pas le dernier modèle, il n'a ni bluetooth ni wifi d'intégré cependant pour ce que je souhaite en faire, cela me suffit amplement. Le besoin pour le wifi pourra se régler avec un dongle.

Ça fait aussi longtemps que je regrette n'avoir jamais fini des jeux comme Chrono Cross ou Breath of Fire 4 et encore moins commencé de gros titres comme Star Ocean, Grandia ou encore Vagrant Story. On va donc partir sur l'installation d'une distribution dédiée à l'émulation. Pourquoi Retropie et pas [Recalbox](https://www.recalbox.com){:target="_blank"} ou encore [Lakka](http://www.lakka.tv){:target="_blank"} ? Il y a pas mal de choix. Le mien s'est porté sur la possibilité et la simplicité d'utiliser [Kodi](https://kodi.tv){:target="_blank"} en parallèle et Retropie le gère très bien.

Retropie est donc un système d'exploitation basé sur Raspbian et utilise EmulationStation pour l'interface ainsi que RetroArch pour l'émulation des consoles.

Commençons donc.

## Configuration requise
Afin de pouvoir procéder à l'installation, il nous faut du matériel:

- Un raspberry pi (toutes les versions sont supportées)
- Une alimentation capable de délivrer au moins 2 ampères
- Une micro SD et un adaptateur micro SD > SD
- Un câble HDMI et un écran
- Un ordinateur avec un lecteur de carte SD

## Installation
Il est possible d'installer manuellement Retropie sur une Raspbian déjà installée. Ma carte SD étant vierge, on va installer Retropie directement.

On va récupérer l'image qui correspond à notre Raspberry [ici](https://retropie.org.uk/download/){:target="_blank"}

Une fois téléchargé, on vérifie son intégrité. Je suis sur OSX donc la commande est `md5`. Pour Linux, c'est `md5sum`.
```bash
$ md5 retropie-4.4-rpi2_rpi3.img.gz
MD5 (retropie-4.4-rpi2_rpi3.img.gz) = 56988addb60361a2257a61c69d9fceac
```

On compare le résultat `56988addb60361a2257a61c69d9fceac` avec celui fournit sous le lien du téléchargement de l'image. Si le hash n'est pas le même, le fichier est corrompu et a du mal être téléchargé. Il faudra le refaire.

On extrait l'archive.

```bash
gunzip retropie-4.4-rpi2_rpi3.img.gz
```

Nous voilà donc avec une image .iso.

Ici, plusieurs choix, on peut utiliser `dd` mais on a un moyen plus simple: [Etcher](https://www.balena.io/etcher/){:target="_blank"}

On insère la micro SD dans l'ordinateur et on lance Etcher. Rien de bien compliqué, on va séléctionner l'image de Retropie, notre SD et on lance l'installation. Ça peut prendre quelques minutes.

Une fois l'installation terminée, on éjecte la SD proprement via le Finder (ou `umount`) et on l'insère dans le rasbperry pi.

Il ne reste plus qu'à brancher l'écran et l'alimentation. Le bootscreen devrait s'afficher:

![Retropie bootscreen]({attach}/images/1-retropie/retropie-bootscreen.jpg)

Et voilà :)
